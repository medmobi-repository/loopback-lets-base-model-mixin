'use strict'

var deprecate = require('depd')('loopback-lets-base-model-mixin');
var baseModel = require('./base-model');

module.exports = function mixin(app) {
    app.loopback.modelBuilder.mixins.define = deprecate.function(app.loopback.modelBuilder.mixins.define,
        'app.modelBuilder.mixins.define: Use mixinSources instead ' +
        'see https://bitbucket.org/letscomunicadev/loopback-lets-base-model-mixin#mixinsources');
    app.loopback.modelBuilder.mixins.define('BaseModel', baseModel);
};